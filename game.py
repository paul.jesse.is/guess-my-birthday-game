from random import randint


name = input("Hi! What's your name? ")
guess_count = 0

for guess in range(5):
    guess_count += 1
    print(f"Guess {guess_count} : {name}, were you born in {randint(1, 12)} / {randint(1924, 2004)}?")
    user_response = input("Yes or no?: ")
    if user_response == "yes":
        print("I knew it!")
        break
    elif user_response == "no":
        print("Drat! Let me try again.")
    else:
        print("Sorry, I don't understand that. Please type yes or no.")

if guess_count == 5:
    print("I have other things to do. Good Bye.")
